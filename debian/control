Source: tmux-themepack-jimeh
Section: utils
Priority: optional
Maintainer: Jonathan Carter <jcc@debian.org>
Build-Depends: debhelper (>= 13)
Standards-Version: 4.6.0
Homepage: https://github.com/jimeh/tmux-themepack
Vcs-Git: https://salsa.debian.org/debian/tmux-themepack-jimeh.git
Vcs-Browser: https://salsa.debian.org/debian/tmux-themepack-jimeh
Rules-Requires-Root: no

Package: tmux-themepack-jimeh
Architecture: all
Depends: tmux, ${misc:Depends}
Suggests: fonts-powerline
Description: pack of various themes for tmux by jimeh
 basic theme and powerline themes for tmux.
 .
 The fonts-powerline package is required on your local machine
 in order to render the powerline glyphs properly.
 .
 To enable a theme, you can add a line like the following to your .tmux.conf:
 .
 source-file "/usr/share/tmux/theme-pack-jimeh/powerline/double/cyan.tmuxtheme"
 .
 You can substitute 'cyan' with blue, gray, green, magenta, orange, red or
 yellow.
 .
 You can also substitute 'double' with 'block' to have straight lines on the
 window list.
